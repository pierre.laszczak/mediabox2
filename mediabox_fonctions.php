<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

/**
 * Class du sélecteur par défaut qui déclenche l'ouverture de la mediabox
 *
 // define('_MEDIABOX_SELECTEUR', '.mediabox');
**/

/**
 *	Désactiver le chargement des CSS côté public
 *
 // define('_MEDIABOX_SKIN_SANS_CSS', true);
**/

/*
 * Sélection lib + skin par défaut :
 *  -  pour le public, avant qu'une skin n'ait été choisie dans le formulaire
 *  -  pour le privé (NB: le dossier doit être copié dans prive/`lib`/`skin`/)
 *
**/
if (defined('_DIR_PLUGIN_FEATHERLIGHT')) {
	defined('_MEDIABOX_SKIN') OR define('_MEDIABOX_SKIN', 'featherlight|plume');
}
if (defined('_DIR_PLUGIN_COLORBOX')) {
	defined('_MEDIABOX_SKIN') OR define('_MEDIABOX_SKIN', 'colorbox|white-shadow');
}

/**
 * Les sous-plugins qui souhaitent modifier
 * ces options doivent passer par le pipeline mediabox_config
 */
function mediabox_config($public = null) {

	include_spip('inc/filtres');
	include_spip('inc/config');

	$prive = ((is_null($public) and test_espace_prive()) 
			or $public === false 
			or $public ==='prive') ? true : false ;

	$selecteur_commun = 
		defined('_MEDIABOX_SELECTEUR') 
			? _MEDIABOX_SELECTEUR 
			: '.mediabox';

	$lib_defaut = table_valeur(explode('|', _MEDIABOX_SKIN),0);
	$skin_defaut = table_valeur(explode('|', _MEDIABOX_SKIN),1);						

	$config = lire_config('mediabox', array());

	$config = array_merge(array(
		'active' => 'oui',
		'auto_detect' => 'oui',
		'traiter_toutes_images' => 'oui',
		'selecteur_commun' => $selecteur_commun,
		'selecteur_galerie' => '.documents_portfolio, .album, .portfolio-albums',
		'splash_url' => '',
		'maxWidth' => '1200px',
		'minWidth' => '300px',
		'_libs' => array(), // liste des libs déclarées ; sera complétée par le pipeline
		'lib' => $lib_defaut,
		'skin' =>  $skin_defaut,
	), $config);

	// transmettre la configuration aux sous-plugins
	$config =  pipeline('mediabox_config', $config);

	// desactiver de force le chargement des CSS
	if (defined('_MEDIABOX_INSERT_HEAD_CSS') && !boolval('_MEDIABOX_INSERT_HEAD_CSS')) {
		$config['skin'] = 'none';
	};

	// desire-t-on le contexte de l'espace privé ?
	if ($prive) {
		$config = array_merge($config, array(
			'active' => 'oui',
			'selecteur_commun' => 'a.popin, .iconifier a[href$=jpg], .iconifier a[href$=png], .iconifier a[href$=gif], ' . $selecteur_commun,
			'selecteur_galerie' => '.portfolios',
			'splash_url' => '',			
			'lib' => $lib_defaut, // forcer la valeur
			'skin' =>  $skin_defaut, // forcer la valeur
		));
	}

	// gérer aussi les liens internes de SPIP
	if (!$prive and $config['splash_url']) {
		include_spip('inc/filtres_ecrire');
		$config['splash_url'] = url_absolue(extraire_attribut(lien_article_virtuel($config['splash_url']), 'href'));
	}

	// prefs explicites du theme
	if (include_spip( ( $prive ? 'prive/' : ' ') . $config['lib'] . '/' . $config['skin'] . '/mediabox_config_theme')) {
		$config_theme = mediabox_config_theme();
		$config = array_merge($config,$config_theme);
	}

	return $config;
}

/**
 * Retourne un tableau d'adresses de fichiers trouvés ou produits, voire directement les balises <script> et/ou <link>
 *
 * Les chemins sont cherchés dans le path de SPIP
 *
 * @uses produire_fond_statique()
 * @uses trouver_fond()
 *
 * @param array|string $files
 *     - array : Liste de fichiers
 *     - string : fichier ou fichiers séparés par `|`
 * @param string $dir
 *     - un dossier optionnel ou faire le recherche
 * @param boolean $ecrire_balise
 *     - options pour envoyer soit un tableau, soit le flux avec balises
 * @return array|string
 *     - array : emplacements des fichiers trouvés ou produits
 *     - string : un flux des balises pour le head  
 **/
function trouver_ou_produire($files, $dir='', $ecrire_balise=false) {
	$liste = array();
	$flux = '';

	foreach (is_array($files) ? $files : explode("|", $files) as $file) {
		if (!is_string($file)) {
			continue;
		}
		$extension = pathinfo($file, PATHINFO_EXTENSION);
   	if (!preg_match('^(css|js)$',$extension))	{
			continue;
   	}  	
		$f = trouver_fond($file,$dir) ? produire_fond_statique($file,array('format'=>$extension)) : find_in_path($file,$dir);
		if ($f) {
			if ($ecrire_balise) {
				if ($extension == 'css') {
					$flux .= "\n" . '<link rel="stylesheet" href="' . $f . '" type="text/css" media="all" />';
				}
				if ($extension == 'js') {
					$flux .= "\n" . '<script type="text/javascript" src="' . $f . '"></script>';
				}
			}
			else {
				$liste[] = $f;
			}
		}
	}
	return $ecrire_balise ? $flux : $liste;
}

// eviter une erreur compilo qd on utilise couleur_hex_to_dec comme filtre
function filtre_couleur_hex_to_dec($couleur) {	
	if (!function_exists('_couleur_hex_to_dec')) {
		include_spip('inc/filtres_images_lib_mini');
	}
	return _couleur_hex_to_dec($couleur);
}

// fonction pour normaliser un nom de dossier
function mediabox_slugify($texte) {
	$texte = strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($texte, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
	return $texte;	
}
